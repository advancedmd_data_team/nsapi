import unittest
from nsapi.nsapi import NetSuiteAPI
import requests_mock
import pandas as pd


class TestNsApi(unittest.TestCase):
    # Test values
    nlauth_account = nlauth_email = nlauth_role = nlauth_signature = 'test'

    @requests_mock.mock()
    def test_happy_path_request_run_all(self, mock_requests):
        """
        The NetSuiteAPI.sequential_request() function will call a given endpoint (i.e. SavedSearch), requesting the
        next 1000 records until no more data can be retrieved.
        """
        ssid = 5555
        first_message = '{"mock_hello": "mock_world", "status_code": "200", "status": "Success", ' \
                        '"payload": [{"col1": "hello", "col2": "world"}]}'
        final_message = '{"status_code": "200", "status": "Failure", "message": "GET request threw error(s)."}'

        nsapi = NetSuiteAPI(nlauth_account=self.nlauth_account,
                            nlauth_email=self.nlauth_email,
                            nlauth_role=self.nlauth_role,
                            nlauth_signature=self.nlauth_signature)

        first_url = nsapi.template_url \
            .replace('{ACCOUNT_ID}', self.nlauth_account) \
            .replace('{SAVED_SEARCH_ID}', str(ssid))\
            .replace('{PAGE_NUMBER}', "1")

        final_url = nsapi.template_url \
            .replace('{ACCOUNT_ID}', self.nlauth_account) \
            .replace('{SAVED_SEARCH_ID}', str(ssid))\
            .replace('{PAGE_NUMBER}', "2")

        # Create a fake endpoint @ good_url and specify the response
        mock_requests.get(first_url, text=first_message)

        # Create a fake endpoint @ end_url and specify the response
        mock_requests.get(final_url, text=final_message)

        output_df = nsapi.sequential_request(saved_search_id=ssid)

        # Expect output_df to be pd.DataFrame
        self.assertTrue(isinstance(output_df, pd.DataFrame),
                        f"\nExpected: type(output_df) = pd.Dataframe"
                        f"\nReceived: type(output_df) = {type(output_df)}")

    @requests_mock.mock()
    def test_happy_path_request_run_once(self, mock_requests):
        """
        The NetSuiteAPI.sequential_request() function will call a given endpoint (i.e. SavedSearch), requesting the
        next 1000 records until no more data can be retrieved.
        """
        ssid = 5555
        first_message = '{"mock_hello": "mock_world", "status_code": "200", "status": "Success", ' \
                        '"payload": [{"col1": "hello", "col2": "world"}]}'
        final_message = '{"status_code": "200", "status": "Failure", "message": "GET request threw error(s)."}'

        nsapi = NetSuiteAPI(nlauth_account=self.nlauth_account,
                            nlauth_email=self.nlauth_email,
                            nlauth_role=self.nlauth_role,
                            nlauth_signature=self.nlauth_signature)

        first_url = nsapi.template_url \
            .replace('{ACCOUNT_ID}', self.nlauth_account) \
            .replace('{SAVED_SEARCH_ID}', str(ssid))\
            .replace('{PAGE_NUMBER}', "1")

        final_url = nsapi.template_url \
            .replace('{ACCOUNT_ID}', self.nlauth_account) \
            .replace('{SAVED_SEARCH_ID}', str(ssid))\
            .replace('{PAGE_NUMBER}', "2")

        # Create a fake endpoint @ good_url and specify the response
        mock_requests.get(first_url, text=first_message)

        # Create a fake endpoint @ end_url and specify the response
        mock_requests.get(final_url, text=final_message)

        output_df = nsapi.sequential_request(saved_search_id=ssid, run_all=False)

        # Expect output_df to be pd.DataFrame
        self.assertTrue(isinstance(output_df, pd.DataFrame),
                        f"\nExpected: type(output_df) = pd.Dataframe"
                        f"\nReceived: type(output_df) = {type(output_df)}")
