import requests
import pandas as pd
import json
import logging
import time
logging.basicConfig(level=logging.INFO)


class NetSuiteAPI:
    def __init__(self, nlauth_email, nlauth_role, nlauth_account, nlauth_signature):

        self.nlauth_email = nlauth_email
        self.nlauth_role = nlauth_role
        self.nlauth_account = nlauth_account
        self.nlauth_signature = nlauth_signature

        # Verify that all of these are either:
        #   - passed in by the calling function, or
        #   - set as environment variables
        if not all((self.nlauth_email, self.nlauth_role, self.nlauth_account, self.nlauth_signature)):
            raise EnvironmentError("NetSuite auth arguments (i.e. nlauth_*) cannot be null.")

        self.headers = {
            'Authorization': f'NLAuth nlauth_email={nlauth_email}, '
                             f'nlauth_role={nlauth_role}, '
                             f'nlauth_account={nlauth_account}, '
                             f'nlauth_signature={nlauth_signature}',
            'Content-Type': 'application/json'
        }

        self.template_url = "https://{ACCOUNT_ID}.restlets.api.netsuite.com/app/site/hosting/restlet.nl?" \
            + "script=customscript_gateway_restlet_api_2_0&deploy=1&v=3&rt=savedsearch&ssid={SAVED_SEARCH_ID}" \
            + "&ppg=1000:{PAGE_NUMBER}"

    def sequential_request(self, saved_search_id, max_retries=5, timeout=500, run_all=True):
        """
        Collect & concatenate each successful NetSuite API response.
        The NetSuite API seems to have a hard record max of 1000 records per response.
        Additional records are paginated and must be retrieved via an additional request.

        IMPORTANT : the request will not throw an exception if the total request time exceeds the timeout period.
        Rather, it will throw an exception if no data is received within the given timeout period.
        """
        page_number = 1
        retry_count = 1
        partial_url = self.template_url \
            .replace('{ACCOUNT_ID}', str(self.nlauth_account))\
            .replace('{SAVED_SEARCH_ID}', str(saved_search_id))

        response_df_list = []

        while True and retry_count <= max_retries:
            # Add the current page number
            current_page_url = partial_url.replace('{PAGE_NUMBER}', str(page_number))

            # Catch connection errors
            try:
                start = time.time()
                # Make request
                response = requests.get(url=current_page_url, headers=self.headers, timeout=timeout)
                end = time.time()
                request_time_elapsed = end - start

            except:
                logging.warning(f"Connection Error: Did not hear back from the server in "
                                f"timeout={timeout} seconds. (retry={retry_count})")
                retry_count += 1
                continue

            # Response received
            response_dict = json.loads(response.text)

            # if request returns FAIL response...
            if response.status_code != 200 or response_dict.get('status') != 'Success':
                # This is the LAST call of a request
                if response_df_list and response_dict.get('message') == 'GET request threw error(s).':
                    pass

                else:
                    # received bad response
                    logging.critical(f"\nExpected: response.status_code = 200"
                                     f"\nReceived: response.status_code = {response.status_code}\n")
                    logging.critical(f"\nExpected: response_dict['status'] = 'Success'" +
                                     f"\nReceived: response_dict['status'] = {response_dict.get('status')}\n")
                    logging.critical(response_dict)
                    break

            # if request returns PASS response...
            else:
                logging.info(f"(SSID: {saved_search_id}) Request Succeeded for page {page_number} in " +
                             f"{round(request_time_elapsed)} seconds.")

                # Add the response to `response_list` and continue looping...
                response_df_list.append(pd.DataFrame(response_dict.get('payload')))
                page_number += 1

                # reset the retry_count to 0 after each success
                retry_count = 1

                # Only continue looping if the `run_all` flag is True; otherwise, only run once
                if run_all:
                    continue

            # return the concatenation of all of the dataframes in `response_df_list`
            logging.info("Request completed.")
            return pd.concat(response_df_list)
