from setuptools import setup, find_packages

setup(name='nsapi',
      version='0.0.2',
      description='A simplified way of connecting to / getting data from NetSuite via the API.',
      url='https://gitlab.com/advancedmd_data_team/nsapi',
      author='David Ostler',
      author_email='david.ostler@advancedmd.com',
      license=None,
      packages=find_packages(exclude=['tests', '.idea', '.cache', '__pycache__']),
      install_requires=[],
      include_package_data=True,
      zip_safe=False)
