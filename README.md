# NetSuite API (a.k.a. nsapi)  [![pipeline status](https://gitlab.com/advancedmd_data_team/nsapi/badges/master/pipeline.svg)](https://gitlab.com/advancedmd_data_team/nsapi/commits/master)
A simplified way of connecting to / getting data from NetSuite via the API.

## Installation
nsapi can be installed by running this in the terminal. It will be installed into the active Python environment.
```bash
pip install -U git+https://gitlab.com/advancedmd_data_team/nsapi.git
```

## Usage
Assuming you have set your NetSuite environment variables, here is how to use it:
```python
from nsapi import NetSuiteAPI
 
df = NetSuiteAPI().sequential_request(saved_search_id=12345)
```
